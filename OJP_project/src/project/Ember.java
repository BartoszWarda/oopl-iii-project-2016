package project;

public class Ember extends Offensive{
	private final String element = "FIRE";
	
	@Override
	public String getElement(){
		return this.element;
	}

	@Override
	public int useAbility(int power, Ability enemyAbility) {
		String enemyElement = enemyAbility.getElement();
		int dmg = power;
		if(enemyElement=="GRASS")
			dmg*=2;
		if(enemyElement=="WATER")
			dmg/=4;
		return dmg;
	}
	
	@Override
	public String toString(){
		return "Ember, element " + this.element;
	}
}
