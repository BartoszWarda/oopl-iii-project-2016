package project;

import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class You extends Character{
	private HashMap<Integer,Offensive>	offensiveAbilities = new HashMap<>();
	private HashMap<Integer,Deffensive>	deffensiveAbilities = new HashMap<>();
	Scanner keyboard = new Scanner(System.in);	
	
	public You(int hitPoints, int power) {
		super(hitPoints, power);
		Random rand = new Random();
		for(int i=0;i<(rand.nextInt(this.hitPoints/2) + this.hitPoints/4);i++)
			offensiveAbilities.put(i+1,this.randomOffensive());		
		for(int i=0;i<(rand.nextInt(this.hitPoints/2) + this.hitPoints/4);i++)			
			deffensiveAbilities.put(i+1, this.randomDeffensive());		
	}

	public void fight(Character enemy){		
		while(this.hitPoints>0 && enemy.hitPoints>0){
			System.out.println("YOU\n" + this+"\nENEMY\n" + enemy);
			try {
				this.off(enemy);
			} catch (DoesntExistException e) {				
				e.printStackTrace();
			}
			if(enemy.hitPoints!=0){
				System.out.println("YOU\n" + this+"\nENEMY\n" + enemy);
				try {
					enemy.off(this);
				} catch (DoesntExistException e) {					
					e.printStackTrace();
				}
				this.check();
			}
		}
		if(this.hitPoints==0)
			System.out.println("You have lost.");
		else
			System.out.println("You have won.");
	}
	
	public void check(){
		if(this.offensiveAbilities.isEmpty()||this.deffensiveAbilities.isEmpty())
			this.hitPoints = 0;
	}

	@Override
	public void off(Character enemy) throws DoesntExistException{		
		System.out.println("Which ability you want to use?");
		for (HashMap.Entry<Integer, Offensive> iterator : offensiveAbilities.entrySet()) {			
			System.out.println(iterator.getKey()+". "+ iterator.getValue());
		}			
		int choice= keyboard.nextInt();		
		Offensive off = offensiveAbilities.get(choice);
		if(off==null){
			throw new DoesntExistException();
		}
		offensiveAbilities.remove(choice);
		System.out.println("You have used " + off);
		int dmg = off.useAbility(this.power, enemy.deff());
		enemy.dmgTaken(dmg);
		System.out.println("You have dealt " + dmg + " damage");
	}

	@Override
	public Deffensive deff() throws DoesntExistException{
		System.out.println("Which ability you want to use?");
		for (HashMap.Entry<Integer, Deffensive> iterator : deffensiveAbilities.entrySet()) {			
			System.out.println(iterator.getKey()+". "+ iterator.getValue());
		}			
		int choice= keyboard.nextInt();		
		Deffensive deff = deffensiveAbilities.get(choice);
		if(deff==null){
			throw new DoesntExistException();
		}
		deffensiveAbilities.remove(choice);	
		System.out.println("You have used " + deff);
		return deff;
	}
}
