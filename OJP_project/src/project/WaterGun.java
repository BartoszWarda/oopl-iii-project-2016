package project;

public class WaterGun extends Offensive{
private final String element = "WATER";
	
	@Override
	public String getElement(){
		return this.element;
	}
	
	@Override
	public int useAbility(int power, Ability enemyAbility) {
		String enemyElement = enemyAbility.getElement();
		int dmg = power;
		if(enemyElement=="FIRE")
			dmg*=2;
		if(enemyElement=="GRASS")
			dmg/=4;
		return dmg;
	}
	
	@Override
	public String toString(){
		return "Water Gun, element " + this.element;
	}
}
