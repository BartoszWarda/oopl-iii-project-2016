package project;

public class Fight {

	public static void main(String[] args){
		int hitPoints = 20;
		int power = 4;
		You you = new You(hitPoints,power);
		Enemy enemy = new Enemy(hitPoints,power);
		you.fight(enemy);
	}

}
