package project;

import java.util.Random;

public abstract class Ability implements Comparable<Ability>{
	public abstract String getElement();

	@Override
	public int compareTo(Ability o) {		
		Random rand = new Random();
		return rand.nextInt(10) - 20;
	}
	
}
