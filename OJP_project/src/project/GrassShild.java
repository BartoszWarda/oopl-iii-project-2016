package project;

public class GrassShild extends Deffensive{
private final String element = "GRASS";
	
	@Override
	public String getElement(){
		return this.element;
	}
	
	@Override
	public String toString(){
		return "Grass Shild, element " + this.element;
	}
}
