package project;

public class FireShild extends Deffensive{
private final String element = "FIRE";
	
	@Override
	public String getElement(){
		return this.element;
	}
	
	@Override
	public String toString(){
		return "Fire Shild, element " + this.element;
	}
}
