package project;

public interface Fightable {
	public void off(Character enemy) throws DoesntExistException;
	public Deffensive deff() throws DoesntExistException;
}
