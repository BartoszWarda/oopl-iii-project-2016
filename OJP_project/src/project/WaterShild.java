package project;

public class WaterShild extends Deffensive{
private final String element = "WATER";
	
	@Override
	public String getElement(){
		return this.element;
	}
	
	@Override
	public String toString(){
		return "Water Shild, element " + this.element;
	}
}
