package project;

import java.util.Random;

public abstract class Character implements Fightable{
	protected int hitPoints;
	protected int power;
	
	public Character(int hitPoints, int power){
		this.hitPoints = hitPoints;
		this.power = power;
	}
	
	public int getPower() {
		return power;
	}	
	
	public void dmgTaken(int dmg){
		this.hitPoints-=dmg;
		if(this.hitPoints<0)
			this.hitPoints = 0;
	}
	
	protected Offensive randomOffensive(){
		Random rand = new Random();
		int ability = rand.nextInt(3);
		if(ability==0)
			return new Ember();
		else if(ability==1)
			return new WaterGun();
		else
			return new RazorLeaf();		
	}
	
	protected Deffensive randomDeffensive(){
		Random rand = new Random();
		int ability = rand.nextInt(2);
		if(ability==0)
			return new FireShild();
		else if(ability==1)
			return new WaterShild();
		else
			return new GrassShild();		
	}
	
	@Override
	public String toString(){
		return "HP: "+ this.hitPoints+ " Power: "+ this.power;
		
	}
}
