package project;

import java.util.TreeSet;

public class Enemy extends Character {
	TreeSet<Offensive> offensiveAbilities = new TreeSet<>();
	TreeSet<Deffensive> deffensiveAbilities = new TreeSet<>();
	
	public Enemy(int hitPoints, int power){
		super(hitPoints,power);
		for(int i=0;i<this.hitPoints;i++)
			//offensiveAbilities.add(this.randomOffensive());
			offensiveAbilities.add(new RazorLeaf());
		for(int i=0;i<this.hitPoints;i++)
			deffensiveAbilities.add(this.randomDeffensive());
	}

	@Override
	public void off(Character enemy) {				
		Offensive off = offensiveAbilities.pollLast();		
		int dmg;
		try {
			dmg = off.useAbility(this.power, enemy.deff());
			enemy.dmgTaken(dmg);
			System.out.println("Enemy have used " + off);
			System.out.println("Enemy have dealt " + dmg + " damage");
		} catch (DoesntExistException e) {
			enemy.dmgTaken(this.power);
			e.printStackTrace();
		}		
	}

	@Override
	public Deffensive deff() {		
		Deffensive deff = deffensiveAbilities.pollLast();
		System.out.println("Enemy have used " + deff);
		return deff;
	}

}
