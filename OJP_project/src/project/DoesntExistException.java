package project;

public class DoesntExistException extends Exception {
	
	@Override
	public String getMessage() {
		return "WARNING: abiblity doesn't exist!";
	}
}
