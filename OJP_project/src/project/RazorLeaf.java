package project;

public class RazorLeaf extends Offensive{
private final String element = "GRASS";
	
	@Override
	public String getElement(){
		return this.element;
	}
	
	@Override
	public int useAbility(int power, Ability enemyAbility) {
		String enemyElement = enemyAbility.getElement();
		int dmg = power;
		if(enemyElement=="WATER")
			dmg*=2;
		if(enemyElement=="FIRE")
			dmg/=4;
		return dmg;
	}
	
	@Override
	public String toString(){
		return "Razor Leaf, element " + this.element;
	}
}
